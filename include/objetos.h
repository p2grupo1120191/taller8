//En este archivo ud podra definir los objetos que podrán ser usados 
//con el slab allocator. Declare tambien el constructor y destructor de 
//cada objeto declarado. Abajo se provee un objeto de ejemplo.
//Declare al menos tres objetos distintos

//El constructor siempre recibe un puntero tipo void al tipo de objeto,
//y su tamaño (tal como se declaro el puntero a funcion).
//No olvide implementar las funciones de crear y destruir de cada objeto.

typedef struct ejemplo{
    int a;
    float b[100];
    char *msg;
    unsigned int refcount;
} Ejemplo;

//Constructor
void crear_Ejemplo(void *ref, size_t tamano);

//Destructor
void destruir_Ejemplo(void *ref, size_t tamano);

//TODO: Crear 3 objetos mas

typedef struct casa{
    int puertas;
    int ventanas;
    int habitaciones;    
    int habitantes;
}Casa;

void crear_Casa(void *ref, size_t tamano);

//Destructor
void destruir_Casa(void *ref, size_t tamano);


typedef struct planeta{
    int seres_vivos;
    int ambientes;    
    char *nombre;
}Planeta;

void crear_Planeta(void *ref, size_t tamano);

//Destructor
void destruir_Planeta(void *ref, size_t tamano);

typedef struct avion{
    int pasajeros; 
    char *nombre_piloto;
}Avion;

void crear_Avion(void *ref, size_t tamano);

//Destructor
void destruir_Avion(void *ref, size_t tamano);


