OBJ = obj/slaballoc.o
OBJ_USER = obj/Ejemplo.o obj/Casa.o obj/Avion.o obj/Planeta.o 

all: estatico dinamico

estatico: obj/prueba.o $(OBJ_USER) libslaballoc.a
	$(CC) -static -g obj/prueba.o $(OBJ_USER) lib/libslaballoc.a -o bin/estatico

dinamico: obj/prueba.o $(OBJ_USER) libslaballoc.so
	gcc -L /lib obj/prueba.o $(OBJ_USER) lib/libslaballoc.so -o bin/dinamico

obj/prueba.o: src/prueba.c
	gcc -g -Wall -c -Iinclude/ src/prueba.c -o obj/prueba.o

obj/Ejemplo.o: src/Ejemplo.c
	gcc -g -Wall -c -Iinclude/ src/Ejemplo.c -o obj/Ejemplo.o

obj/slaballoc.o: src/slaballoc.c
	gcc -g -Wall -c -fPIC -Iinclude/ src/slaballoc.c -o obj/slaballoc.o

obj/Casa.o: src/Casa.c
	gcc -g -Wall -c -Iinclude/ src/Casa.c -o obj/Casa.o

obj/Avion.o: src/Avion.c
	gcc -g -Wall -c -Iinclude/ src/Avion.c -o obj/Avion.o

obj/Planeta.o: src/Planeta.c
	gcc -g -Wall -c -Iinclude/ src/Planeta.c -o obj/Planeta.o

#agregue las reglas que necesite

libslaballoc.a: $(OBJ)
	ar rcs lib/libslaballoc.a obj/slaballoc.o

libslaballoc.so: $(OBJ)
	gcc -shared -fPIC obj/slaballoc.o -o lib/libslaballoc.so
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./lib

.PHONY: clean
clean:
	rm bin/* obj/*.o lib/*

