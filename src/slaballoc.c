#include "slaballoc.h"
#include <inttypes.h>

SlabAlloc *crear_cache(char *nombre, size_t tamano_objeto, constructor_fn constructor, destructor_fn destructor){
	//Reservamos memoria para el Slaballoc
	SlabAlloc *tmp= (SlabAlloc *)malloc(sizeof(SlabAlloc));
	if (tmp == NULL) {	
		return NULL;
	}
	//Se guardan los datos del alloc
	tmp->nombre=strdup(nombre);
	tmp->tamano_objeto= tamano_objeto;
	tmp->constructor=constructor;
	tmp->destructor= destructor;
	tmp->tamano_cache= TAMANO_CACHE;
	tmp->cantidad_en_uso=0;
	//Reservamos memoria para el arreglo de los objetos
	void *objetos_arr= malloc(TAMANO_CACHE*tamano_objeto);
	int i;
	for(i=0;i<TAMANO_CACHE;i++) {
		//Construimos los objetos uno por uno,
		//no sabemos que tipo de objeto se guarda en el arreglo por lo que
		//no podemos recorrer el arreglo convencionalmente (es decir usar objeto_arr[i])
		//por ello usamos el tamaño del objeto para movernos por la memoria:
		//objetos_arr+tamano_objeto*i
		//se puede hacer esto ya que son punteros 
		//tamano_objeto*i nos indica el espacio que debemos movernos en cada interacion
		constructor(objetos_arr+tamano_objeto*i,tamano_objeto);	//objetos_arr[i]
	}
	
	//Reservamos memoria para el arreglo de slabs
	Slab *slab_arr= (Slab *)malloc(TAMANO_CACHE*sizeof(Slab));
	
	for(i=0;i<TAMANO_CACHE;i++) {
		//Asignamos a cada slab una direccion un objeto
		slab_arr[i].ptr_data=objetos_arr+tamano_objeto*i;
		//y lo marcamos como disponible
		slab_arr[i].status=DISPONIBLE;
		slab_arr[i].ptr_data_old=NULL;
		
	}
	//guardamos los punteros que apuntan a los arreglos de objetos y slabs en el alloc
	tmp->mem_ptr=objetos_arr;
	tmp->slab_ptr=slab_arr;
	return tmp;
		
	
}

void *obtener_cache(SlabAlloc *alloc, int crecer){
	
	if((alloc->cantidad_en_uso)==(alloc->tamano_cache)){ //comprobamos que haya slabs disponibles
		//si no hay disponibles primero hacemos crecer el cache si la bandera crecer es igual a 1 
		if(crecer==1){			
	
 			//Duplicacion del espacio en el cache
 			int start = alloc->tamano_cache;
 			alloc->tamano_cache*=2;
			Slab *tmp_ptr = realloc(alloc->slab_ptr, alloc->tamano_cache*sizeof(Slab));
			if (tmp_ptr == NULL ) {
				return NULL;
			}
			void * tmp_ptr2 =realloc(alloc->mem_ptr, alloc->tamano_cache*alloc->tamano_objeto);
			if (tmp_ptr2 == NULL) {
				alloc->slab_ptr=tmp_ptr;
				return NULL;
			}
			alloc->slab_ptr=tmp_ptr;
			alloc->mem_ptr=tmp_ptr2;
			//Creacion de los nuevos objetos
			//se crean desde la interacion i=((alloc->tamano_cache/2)-1
			//ya que todos los objetos anteriores ya fueron creados

			// Si va pero seria i=tamanioviejo, mientras i<tamanio_cache
			int i;			
			for(i=start;i<alloc->tamano_cache;i++) {
				alloc->constructor(alloc->mem_ptr+alloc->tamano_objeto*i,alloc->tamano_objeto);						
			}
			
			
			//Reasignacion de Punteros
			for(i=0;i<start;i++) {
				//El ptr_data_old se apunta a la vieja direccion donde apuntaba ptr_data
				alloc->slab_ptr[i].ptr_data_old=alloc->slab_ptr[i].ptr_data;
				//Y el ptr_data se apunta a la nueva direccion
				alloc->slab_ptr[i].ptr_data=alloc->mem_ptr+alloc->tamano_objeto*i;

				//						
			}
			//Asignacion de los nuevos objetos a los nuevos slabs
			for(i=start;i<alloc->tamano_cache;i++) {	
				alloc->slab_ptr[i].ptr_data= alloc->mem_ptr+alloc->tamano_objeto*i;
				alloc->slab_ptr[i].status=DISPONIBLE;
				alloc->slab_ptr[i].ptr_data_old=NULL;				
			}


			
			
		}else{
			//Retornamos null si la andera crecer es 0
			return NULL;
		}		
	}
	//bsqueda de un slab disponible
	Slab *arr_Slabs=alloc->slab_ptr;
	int i;
	for(i=0;i<(alloc->tamano_cache);i++){
		if(arr_Slabs[i].status==DISPONIBLE){// comproamos si el slab esta disponible
			//si esta disponible lo marcamos como en uso y retornamos el objeto al que apunta
			arr_Slabs[i].status=EN_USO;//alloc->slab_ptr[i].status=EN_USO
			alloc->cantidad_en_uso=alloc->cantidad_en_uso+1;
			return arr_Slabs[i].ptr_data;
		}
	}	
	return NULL;
}



void devolver_cache(SlabAlloc *alloc, void *obj){
	Slab *arr_Slabs=alloc->slab_ptr;
	int i;
	for(i=0;i<(alloc->tamano_cache);i++){
		//Buscamos el objeto dentro de los slabs
		if(arr_Slabs[i].ptr_data==obj || (arr_Slabs[i].ptr_data_old==obj && arr_Slabs[i].ptr_data_old!=NULL)){//obj == slab.ptr_data 
			arr_Slabs[i].status=DISPONIBLE;

			/*destruir se procede a eliminar ya que esta mal el comentario de invocarlo en la funcion devolver cache ya que se produce doble free

			alloc->destructor(arr_Slabs[i].ptr_data,alloc->tamano_objeto);
			//alloc->constructor(arr_Slabs[i].ptr_data,alloc->tamano_objeto);//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/


			alloc->cantidad_en_uso=alloc->cantidad_en_uso-1;
			
		}
	}	
}

void destruir_cache(SlabAlloc *cache){
	if(cache->cantidad_en_uso==0){
		void *objetos = cache->mem_ptr;
		int i;
		for(i=0;i<cache->tamano_cache;i++){
			//Se destruyen los objetos uno por uno
			cache->destructor(objetos+cache->tamano_objeto*i, cache->tamano_objeto);			
		}
		//Se libera el espacio donde estaban los objetos 		
		free(cache->mem_ptr);
		free(cache->nombre);
		//luego el de los slabs
		free(cache->slab_ptr);
		//finalmente se libera el espacio del Slaballoc
		free(cache);
		
		//cache=NULL;	
		//return;
		//printf("%p", cache);
			
	}		
}

void stats_cache(SlabAlloc *cache){
	
	if(cache==NULL){
		printf("Error: cache invalido");
		return;		
	}


	printf("Nombre de cache: %s \n", cache->nombre);
	printf("Cantidad de slabs: %i \n", cache->tamano_cache);
	printf("Cantidad de slabs en uso: %i \n", cache->cantidad_en_uso);
	printf("Tamano de objeto: %lu \n\n", cache->tamano_objeto);
	
	printf("Direccion de cache->mem_ptr: %p\n", cache->mem_ptr);
	int i;

	

	for(i=0;i<cache->tamano_cache;i++){
		void *ptr_data=(cache->slab_ptr[i].ptr_data);
		void *ptr_data_old=(cache->slab_ptr[i].ptr_data_old);
		printf("direccion ptr[%i].ptr_data: %p",i,ptr_data);
		if((cache->slab_ptr[i]).status==EN_USO){
			printf(" EN_USO ,");
		}else{
			printf(" DISPONIBLE ,");
		}
		printf(" ptr[%i].ptr_data_old: %p \n",i,ptr_data_old );
	}
	
}


