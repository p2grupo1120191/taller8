#include <unistd.h>
#include <stdlib.h>
#include "../include/objetos.h"


void crear_Planeta(void *ref, size_t tamano){
	Planeta *buf = (Planeta *)ref;
	buf->seres_vivos=0;
	buf->ambientes=0;
	buf->nombre =NULL;
}

//Destructor
void destruir_Planeta(void *ref, size_t tamano){
	Planeta *buf = (Planeta *)ref;
	buf->seres_vivos=-1;
	buf->ambientes=-1;
	buf->nombre=NULL;
}
